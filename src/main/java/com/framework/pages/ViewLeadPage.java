package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ViewLeadPage extends ProjectMethods {
	
	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.ID, using ="viewLead_firstName_sp") WebElement verFirstName;
	@FindBy(how = How.XPATH, using = "//form[@name='qualifyLeadForm']/following::div//a[3]") WebElement editLead;
	@FindBy(id="viewLead_companyName_sp") WebElement verCmpnyName;
	@FindBy(id="viewLead_lastName_sp") WebElement verLastName;
	@FindBy(how=How.LINK_TEXT, using="Duplicate Lead") WebElement duplicateLead;
	public ViewLeadPage verifyFirstName(String firstName) {
		if (firstName.equalsIgnoreCase(getElementText(verFirstName))) {
			System.out.println("First Name Matches");
		}
		return this;
	}
	
	public ViewLeadPage verifyCmpnyName(String cmpnyName) {
		if (cmpnyName.equalsIgnoreCase(getElementText(verCmpnyName))) {
			System.out.println("Company Name Matches");
		}
		return this;
	}
	
	public ViewLeadPage verifyLastName(String lastName) {
		if (lastName.equalsIgnoreCase(getElementText(verLastName))) {
			System.out.println("Last Name Matches");
		}
		return this;
	}
	public EditLeadPage clickEdit() {
		click(editLead);
		return new EditLeadPage();
	}
	public DuplicateLeadPage clickDuplicateLead() {
		click(duplicateLead);
		return new DuplicateLeadPage();
	}
	
}
