package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class FindLeadPage extends ProjectMethods {

	public FindLeadPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy (how = How.XPATH,using="(//div[@class='x-form-element'])[14]/input") WebElement firstName;
	@FindBy (how = How.XPATH,using = "(//td[@class='x-btn-left'])[6]/following-sibling::td//button") WebElement findLead;
	@FindBy(how = How.XPATH,using = "//td[@class='x-grid3-body-cell']/../preceding-sibling::tr//a") WebElement firstLeadRow;
	public FindLeadPage enterFirstName(String data) {
		
		clearAndType(firstName, data);
		
		
		return this;
	}
	
	public FindLeadPage clickFindLead() {
		click(findLead);
		return this;
	}
	
	public ViewLeadPage clickFirstLeadRow() {
		try {
			Thread.sleep(3000);
			click(firstLeadRow);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.out.println("Error in sleep");
		}
		
		return new ViewLeadPage();
	}
	
}
