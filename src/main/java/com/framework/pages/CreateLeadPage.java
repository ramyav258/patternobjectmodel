package com.framework.pages;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {
	
	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy (how = How.ID,using = "createLeadForm_companyName") WebElement cmpnyName;
	@FindBy (how = How.ID,using = "createLeadForm_firstName") WebElement firstName;
	@FindBy (how = How.ID,using = "createLeadForm_lastName") WebElement lastName;
	@FindBy (how = How.NAME,using = "submitButton") WebElement submit;
	
	public CreateLeadPage enterCmpnyName(String data) {
		clearAndType(cmpnyName, data);
		return this;
	}
	public CreateLeadPage enterFirstName(String data) {
		clearAndType(firstName, data);
		return this;
	}
	public CreateLeadPage enterLastName(String data) {
		clearAndType(lastName, data);
		return this;
	}
	public ViewLeadPage clickSubmit() {
		click(submit);
		return new ViewLeadPage();
	}

}
