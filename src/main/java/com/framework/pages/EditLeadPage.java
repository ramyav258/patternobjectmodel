package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class EditLeadPage extends ProjectMethods {
	
	public EditLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="updateLeadForm_firstName") WebElement firstName;
	@FindBy(name="submitButton") WebElement updateButton;
	public EditLeadPage EditFirstName(String data) {
		clearAndType(firstName, data);
		return this;
	}
	
	public ViewLeadPage  clickUpdate() {
		click(updateButton);
		return new ViewLeadPage();
	}

}
