package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC002_EditLead extends ProjectMethods{
	
	@BeforeTest
	public void sendData() {
		testCaseName = "TC002_EditLead";
		testDescription = "Edit Lead";
		testNodes = "Leads";
		author = "Ramya";
		category ="SIT";
		dataSheetName = "TC002";
	}
	
	@Test(dataProvider = "fetchData")
	public void editLead(String username, String password, String firstname, String newFirstname) {
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLead()
		.enterFirstName(firstname)
		.clickFindLead()
		.clickFirstLeadRow()
		.clickEdit()
		.EditFirstName(newFirstname)
		.clickUpdate()
		.verifyFirstName(newFirstname);
	}
	
}
