package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_CreateLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "CreateLead";
		testDescription = "CreateLead";
		testNodes = "Leads";
		author = "Ramya";
		category = "SIT";
		dataSheetName = "TC001";
	}
	
	@Test(dataProvider = "fetchData")
	public void createLead(String username, String password, String firstName, String lastName, String cmpnyName) {
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickCreateLead()
		.enterCmpnyName(cmpnyName)
		.enterFirstName(firstName)
		.enterLastName(lastName)
		.clickSubmit()
		.verifyFirstName(firstName)
		.verifyCmpnyName(cmpnyName)
		.verifyLastName(lastName);
	}
	
}
